//
//  ConstantURL.swift
//  20230328-RasheedAndrew-NYCSchools
//
//  Created by rasheed andrew on 3/28/23.
//

import Foundation


struct Constants {
   
    
    ///whenever i use this url, the data gets fetched and doesn't  return. it is a data for all schools that hasn't been filtered by it's dbn. i get an error of
    ///Error decoding JSON: The data couldn’t be read because it is missing.
    static let highSchoolsURL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    
    
    
    ///whenever i use this url, the data gets fetched and returns. it is a data for a school that has been filtered by it's dbn. It was used on the website as an example
  static let highSchoolsURLFields = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?dbn=02M438"
    
   
    
    ///whenever i use this url, the data gets fetched and returns. it is a data for a SAT test score that has been filtered by it's dbn. It was used on the website as an example
    static let schoolWithSATScoreURL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=11X253"
    
    
    
    ///whenever i use this url, the data gets fetched and returns. it is a data for all SAT test scores.
    static let schoolWithSATScoreURLFields = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
                                              
}

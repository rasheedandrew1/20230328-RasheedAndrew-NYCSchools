//
//  SchoolDirectoryView.swift
//  20230328-RasheedAndrew-NYCSchools
//
//  Created by rasheed andrew on 3/28/23.
//

import SwiftUI

struct SchoolDirectoryView: View {
    
    @StateObject var schoolDirectory = SchoolDirectory()
    @State var showScores : Bool = false
    @State var selectedSchool : String = ""
    
    var body: some View {
        NavigationView {
            VStack {
                Text ("NYC Schools")
                    .font(.system(size: 25))
                    .fontWeight(.heavy)
                List{
                    ForEach(schoolDirectory.satscores, id: \.self) { data in
                        NavigationLink(destination: SchoolDirectoryDetailedView(test: data)) {
                            HStack (spacing: 10) {
                                Image(systemName: "building.columns.fill")
                                    .font(.system(size: 30))
                                    .foregroundColor(.red)
                                
                                Text(data.schoolName)
                                    .font(.system(size: 16))
                                    .fontWeight(.bold)
                                    .foregroundColor(.primary)
                            }
                        }
                    }
                }.listStyle(.plain)
            }
            
        }
    }
}

struct SchoolDirectoryView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolDirectoryView()
    }
}

//
//  SchoolDirectoryDetailedView.swift
//  20230328-RasheedAndrew-NYCSchools
//
//  Created by rasheed andrew on 3/28/23.
//

import SwiftUI

struct SchoolDirectoryDetailedView: View {

    @ObservedObject var schoolDirectory = SchoolDirectory()
    let test : SATScore
    
    
    var body: some View {
        VStack (alignment: .leading, spacing: 20){
                     Text("\(test.schoolName)")
                         .font(.system(size: 18))
                         .fontWeight(.bold)
                     
                     
                     Text("PARTICIPANTS: \(test.numOfSatTestTakers)")
                         .font(.system(size: 14))
                         .fontWeight(.bold)
                     
                     
                     Text("MATH AVG: \(test.satCriticalReadingAvgScore)")
                         .font(.system(size: 14))
                         .fontWeight(.bold)
                         .foregroundColor(.secondary)
                     
                     Text("WRITING AVG: \(test.satMathAvgScore)")
                         .font(.system(size: 14))
                         .fontWeight(.bold)
                         .foregroundColor(.secondary)
                     
                     
                     Text("CRITICAL READING AVG: \(test.satWritingAvgScore)")
                         .font(.system(size: 14))
                         .fontWeight(.bold)
                         .foregroundColor(.secondary)
                     Spacer()
                     
                 }
                 .padding(15)
    }
}

struct SchoolDirectoryDetailedView_Previews: PreviewProvider {
    static var previews: some View {
   //     SchoolDirectoryDetailedView()
        EmptyView()
    }
}

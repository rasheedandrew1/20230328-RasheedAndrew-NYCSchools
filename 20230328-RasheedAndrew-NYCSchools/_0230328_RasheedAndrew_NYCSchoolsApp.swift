//
//  _0230328_RasheedAndrew_NYCSchoolsApp.swift
//  20230328-RasheedAndrew-NYCSchools
//
//  Created by rasheed andrew on 3/28/23.
//

import SwiftUI

@main
struct _0230328_RasheedAndrew_NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            SchoolDirectoryView()
            
        }
    }
}
